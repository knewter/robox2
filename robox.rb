#!/usr/bin/env ruby
$: << File.join(File.dirname(__FILE__), 'lib')
require 'boot'

$debug = false 

client = Robox::Client.new do |bot|
  bot.nickname  = ::Setting.find_by_name('nickname').value
  bot.server    = ::Setting.find_by_name('server').value
  bot.port      = ::Setting.find_by_name('port').value
end

Robox::AutoJoinPlugin.new(client)
Robox::AutoOpPlugin.new(client)
Robox::BasicCommandsPlugin.new(client)
Robox::ChancePlugin.new(client)
Robox::ChannelsPlugin.new(client)
Robox::ConvertPlugin.new(client)
Robox::DefinitionsPlugin.new(client)
Robox::LoggerPlugin.new(client)
Robox::GithubPlugin.new(client)
Robox::OfflineMessagesPlugin.new(client)
Robox::SettingsPlugin.new(client)
Robox::WeatherPlugin.new(client)

client.connect!
