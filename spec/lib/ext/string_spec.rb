require 'spec_helper'
require 'ext/string'

describe String do
  let(:block) { proc { @callback_fired = true } }
  before(:each) { @callback_fired = false }
  %w(privmsg notice join kick mode part quit nick topic).each do |method|
    describe "##{method}?" do
      it "detects a #{method.upcase}" do
        method.upcase.send("#{method}?".to_sym).should be_true
      end

      it "fires a callback when true" do
        method.upcase.send("#{method}?".to_sym, &block)
        @callback_fired.should be_true
      end

      it "should not fire a callback when false" do
        "Banana".upcase.send("#{method}?".to_sym, &block)
        @callback_fired.should be_false
      end
    end
  end

  describe "#ping?" do
    it "detects a PING" do
      "PING ircserver.net".ping?.should be_true
    end

    before(:each) { @callback_return = false }
    it "fires a callback with the server name when true" do
      "PING ircserver.net".ping? {|c| @callback_return = c[1] }
      @callback_return.should == 'ircserver.net'
    end

    it "should not fire a callback when false" do
      "Banana".ping? {|c| @callback_return = c[1] }
      @callback_return.should be_false
    end
  end

  describe "#raw?" do
    it "detects a RAW numeric" do
      "004".raw?.should be_true
    end

    before(:each) { @callback_return = false }
    it "fires a callback with the raw numeric when true" do
      "004".raw? {|c| @callback_return = c[1] }
      @callback_return.should == '004'
    end
    
    it "should not fire a callback when false" do
      "Banana".raw? {|c| @callback_return = c[1] }
      @callback_return.should be_false
    end
  end
end

