require 'spec_helper'
APP_ROOT = File.expand_path(File.join(File.dirname(__FILE__), "..", "..", ".."))
$: << File.join(APP_ROOT, "extensions/convert")

module Robox
  class Client
    def add_hook(*args); end;
  end
  class Plugin
    def initialize(client)
      @client = client
    end
    def add_hook(a, b = nil, &block)
      @hooks = {a => block }
    end
  end
end

require 'convert'

module Robox
  describe ConvertPlugin do
    let(:client) { double "Client" }

    it "should add a command hook" do
      client.should_receive(:add_hook).with(:command)
      plugin = ConvertPlugin.new(client)
    end

    let(:plugin) { ConvertPlugin.new(Client.new) }
    describe "temperature" do
      describe "to celsius" do
        specify "from fahrenheit converts properly" do
          plugin.c_to_f(0).should == 32
          plugin.c_to_f(-40).should == -40
          plugin.c_to_f(100).should == 212
        end

        specify "from kelvin converts properly" do
          plugin.k_to_c(0).should == -273.15
          plugin.k_to_c(233.15).to_i.should == -40
          plugin.k_to_c(273.15).to_i.should == 0
        end
      end
      describe "to fahrenheit" do
        specify "from celsius converts properly" do
          plugin.f_to_c(32).to_i.should == 0
          plugin.f_to_c(-40).to_i.should == -40
          plugin.f_to_c(212).to_i.should == 100
        end

        specify "from kelvin converts properly" do
          plugin.k_to_f(273.15).to_i.should == 32
          plugin.k_to_f(233.15).to_i.should == -40
          plugin.k_to_f(373.15).to_i.should == 210
        end
      end
      describe "to kelvin" do
        specify "from celsius converts properly" do
          plugin.c_to_k(0).to_i.should == 273
          plugin.c_to_k(100).to_i.should == 373
          plugin.c_to_k(-100).to_i.should == 173
        end

        specify "from fahrenheit converts properly" do
          plugin.f_to_k(32).to_i.should == 273
          plugin.f_to_k(-40).to_i.should == 233
          plugin.f_to_k(212).to_i.should == 373
        end
      end
    end

    describe "weight" do
      describe "to kgs" do
        specify "from lbs converts properly" do
          sprintf("%.1f", plugin.lbs_to_kgs(2.2)).to_f.should == 0.9
          plugin.lbs_to_kgs(220).to_i.should == 99
        end
      end
      describe "to lbs" do
        specify "from kgs converts properly" do
          plugin.kgs_to_lbs(1).to_i.should == 2
          plugin.kgs_to_lbs(100).to_i.should == 220
        end
      end
    end

    describe "distance" do
      describe "to miles" do
        specify "from kms converts properly" do
          sprintf("%.1f", plugin.kms_to_miles(1)).to_f.should == 0.6
          plugin.kms_to_miles(160.9).to_i.should == 99 
        end
      end
      describe "to kms" do
        specify "from miles converts properly" do
          sprintf("%.1f", plugin.miles_to_kms(1)).to_f.should == 1.6
          plugin.miles_to_kms(62.1).to_i.should == 99
        end
      end
    end
  end
end
