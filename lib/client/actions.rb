module Robox
  module Actions

    def join(channel)
      reply "JOIN #{channel}"
      did_join_channel!(channel)
    end

    def part(channel)
      reply "PART #{channel}"
      did_part_channel!(channel)
    end
    
    def say(target, message)
      reply "PRIVMSG #{target} :#{message}"
    end
    
    def set_mode(modestring)
      reply "MODE #{modestring}"
    end

    def op(user, channel)
      set_mode "#{channel} +o #{user}"
    end

    def voice(user, channel)
      set_mode "#{channel} +v #{user}"
    end
  end
end
