module Robox
  module Hooks
    
    # def add_hook
    #   params:
    #     event: the callback event to register hook to
    #     name: the internal address of the hook
    #     block: the code to be executed upon callback
    def add_hook(event, name = reserve_hook_id, &block)
      # If there's already an existing hook for this event, append it
      if @hooks.has_key? event
        @hooks[event].merge!({name.to_sym => block})
      else
        @hooks[event] = {name.to_sym => block}
      end
      
      debug "Adding hook #{event}: #{name}"
    end
    
    # remove_hook
    #   params:
    #     event: the event in which the hook is registered
    #
    # TODO: Make this remove any key under any event, rather than a specific event
    #
    # Removes a registered hook.
    def remove_hook(event, name)
      @hooks[event].delete(name.to_sym)
    end
      
    private
      # def reserve_hook_id
      #
      # Used to generate a name for a hook that is then used to refer to a hook
      # inside the bot-wide @@hooks registry. Kept track of on the instance level
      # so that a plugin can register / unregister its own hooks dynamically 
      # (i.e. plugin hot-loading).
      def reserve_hook_id
        @hooks_registered ||= []
        @hook_count ||= 0
        
        name = "#{self.class.to_s}_#{@hook_count}"
        @hook_count += 1
        @hooks_registered << name
        
        name
      end
      
      # run_hook
      #   params:
      #     event: the callback event to trigger
      #     data: the information that should be passed to the hooks, if any
      #
      # Triggers all hooks under an event.
      def run_hook(event, data = {})
        debug "Running hooks for #{event} with data #{data.inspect}."
        if @hooks.has_key? event
          @hooks[event].clone.each_pair do |name, block| 
            debug "Running #{name}."
            data.empty? ? block.call : block.call(data)
          end
        end
      end
      alias_method :trigger, :run_hook

  end
end
