class Channel < ActiveRecord::Base

  class << self
    def autojoins
      where :autojoin => true
    end
  end

end
