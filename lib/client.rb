require 'socket'
require 'logging'

Dir[File.join(File.dirname(__FILE__), 'client', '*.rb')].each {|f| require f }

module Robox
  class Client
    include Logging
    include Robox::Hooks
    include Robox::Events
    include Robox::Actions
    include Robox::Raws

    attr_accessor :options, :server, :port, :nickname, :socket

    def initialize(socket = nil)
      yield self if block_given?
      @hooks = {}
    end

    def connect!
      @nickname   ||= "Robox"
      @server     ||= 'irc.freenode.net'
      @port       ||= 6667
      @socket     ||= ::TCPSocket.new(@server, @port)

      reply "USER #{@nickname} #{@nickname} #{@nickname} #{@nickname}"
      reply "NICK #{@nickname}"
      did_connect!
      parse!
    end
    
    def parse(message)
      message.strip!
      log message
      did_receive_response!(message)

      message.ping? { |m| reply "PONG #{m[1]}" } and return

      message = message.sub(':', '').split(" ")
      sender, raw, target = message.shift(3)
      user, hostname = sender.split("!")
      message.first.sub!(':', '') unless message.first.nil?
      highlight message.inspect

      
      raw.raw? do |m| 
        send "raw_#{raw}", raw if respond_to? "raw_#{raw}"
        did_receive_raw! raw
      end and return
      
      raw.privmsg? { did_receive_privmsg! target, user, message.join(' ') } and return
      raw.notice? { did_receive_notice! user, message.join(' ') } and return
      raw.join? { did_receive_join!(user, target) if user != @nickname } and return
      raw.kick? { did_receive_kick! target, user, message.shift, message.join(' ') } and return
      raw.mode? { did_receive_mode_change! target, message } and return
      raw.part? { did_receive_part! user, target } and return
      raw.quit? { did_receive_quit! user } and return
      raw.nick? { did_receive_nick_change! user, target } and return
      raw.topic? { did_receive_topic_change! target, message } and return
    end
    
    def parse!
      loop do
        parse @socket.gets
      end
    end
    
    def reply(message)
      chat ">> #{message}"
      @socket.write message + "\n"
    end
    
  end
end


