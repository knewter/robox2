module Robox
  module IRCStringExtensions
    def ping?
      if (matches = /\APING (.+?)\Z/.match(self)) && block_given?
        yield matches
      end

      !!matches
    end 
  
    def raw?
      if (matches = /(\d{3})/.match(self)) && block_given?
        yield matches
      end

      !!matches
    end
  
    def privmsg?(&block)
      yield_and_return_match(self == 'PRIVMSG', &block)
    end
  
    def notice?(&block)
      yield_and_return_match(self == 'NOTICE', &block)
    end
  
    def join?(&block)
      yield_and_return_match(self == 'JOIN', &block)
    end
  
    def kick?(&block)
      yield_and_return_match(self == 'KICK', &block)
    end
  
    def mode?(&block)
      yield_and_return_match(self == 'MODE', &block)
    end
  
    def part?(&block)
      yield_and_return_match(self == 'PART', &block)
    end
  
    def quit?(&block)
      yield_and_return_match(self == 'QUIT', &block)
    end
  
    def nick?(&block)
      yield_and_return_match(self == 'NICK', &block)
    end
  
    def topic?(&block)
      yield_and_return_match(self == 'TOPIC', &block)
    end

    def yield_and_return_match(condition)
      yield if condition && block_given?
      condition
    end
    private :yield_and_return_match

  end
end

String.send(:include, Robox::IRCStringExtensions)
