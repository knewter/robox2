module Robox
  class GithubPlugin < Plugin

    def initialize(client)
      super(client)
      @client.add_hook(:command) {|m| route m[:target], m[:command], m[:user] }
    end

    def route(target, command, user)
      /\Arepo (.+?)\Z/i.match command do |m|
        @client.say target, "#{user}: https://github.com/#{m[1]}"
      end
    end
  end
end
