module Robox
  class DefinitionsPlugin < Plugin
    def initialize(client)
      super(client)
      @client.add_hook(:command) {|m| route m[:target], m[:command], m[:user] }
    end

    def route(target, command, user)
      /\Adef(ine)? (.+?) (as|is) (.+?)\Z/i.match(command) do |m|
        create_or_update_definition(m[2], m[4])
        @client.say target, "#{user}: Okay, #{m[2]} is #{m[4]}."
      end and return

      /\A\!(.+?)\??\Z/i.match(command) do |m|
        definition = find_definition(m[1])
        if definition
          @client.say target, "#{user}: #{m[1]} is #{definition.value}"
        else
          @client.say target, "#{user}: Sorry, but I don't know what you're talking about."
        end
      end and return

      /\Aforget (.+?)\Z/i.match(command) do |m|
        definition = find_definition(m[1])
        definition.destroy

        @client.say target, "#{user}: Okay, I've forgotten about #{m[1]}."
      end and return
    end

    def create_or_update_definition(k, v)
      definition = ::Definition.find_or_initialize_by_name(k.to_s)
      highlight definition.inspect
      definition.update_attributes!(:value => v.to_s)
    end

    def find_definition(k)
      ::Definition.find_by_name(k.to_s)
    end
  end
end
