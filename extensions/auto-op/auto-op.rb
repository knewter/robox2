module Robox
  class AutoOpPlugin < Plugin
    
    def initialize(client)
      super(client)
      @client.add_hook(:join) {|m| @client.op(m[:user], m[:channel]) }
    end
    
  end
end
