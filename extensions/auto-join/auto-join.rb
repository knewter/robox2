module Robox
  class AutoJoinPlugin < Plugin
    def initialize(client)
      super(client)
      @client.add_hook(:startup) {|m| ::Channel.autojoins.map(&:name).each {|c| @client.join c } }
    end
  end
end
