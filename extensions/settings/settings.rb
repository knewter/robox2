module Robox
  class SettingsPlugin < Plugin

    def initialize(client)
      super(client)
      @client.add_hook(:command) {|m| route m[:target], m[:command] }
    end

    ################################################################
    public
    ################################################################
    
    def route(target, command)
      # If we don't match the namespace and the possible command, terminate ASAP.
      return false unless /config (get|set|list)/i.match command

      if $1.downcase == 'list'
        list(target)
        return true
      end
      
      # Strip the first two words from the argument.
      command = command.split(' ').slice(2..-1).join(' ')

      if $1.downcase == 'get'
        get(target, command)
      else
        key, new_value = command.split(' ')
        set(target, key, new_value)
      end
    end

    ################################################################  
    private
    ################################################################
    
    def list(target)
      @client.say target, "Valid config settings: #{::Setting.all.collect(&:name).join(", ")}"
    end
    
    def get(target, name)
      setting = ::Setting.find_by_name(name)
      if setting.nil?
        no_such_key(target, name)
      else
        @client.say target, "#{setting.name} => #{setting.value}"
      end
    end
    
    def set(target, key, new_value)
      setting = ::Setting.find_by_name(key)
      if setting.nil?
        no_such_key(target, key)
      else
        setting.update_attribute('value', new_value)
        @client.say target, "Updated setting."
      end
    end
    
    def no_such_key(target, key)
      @client.say target, "No such setting by that key(#{key})."
    end
    
  end
end
