class CreateChannels < ActiveRecord::Migration
  def self.up
    create_table :channels do |t|
      t.string :name
      t.boolean :autojoin, :default => false
    end
  end
  
  def self.down
    drop_table :settings
  end
end
